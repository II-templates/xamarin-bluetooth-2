﻿using Plugin.BluetoothLE;
using Plugin.BluetoothLE.Server;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace bluetooth2
{
	public partial class MainPage : ContentPage
	{



        




		public MainPage()
		{
			InitializeComponent();

            MessagingCenter.Subscribe<MainPageViewModel>(this, "AlertWarningPowerOff", _ => { 
                DisplayAlert ( "Attenzione", "Bluetooth spento", "ok" );                
            });

            BindingContext = new MainPageViewModel();
		}



        private void Scan_Clicked(object sender, EventArgs e) {


            if ( CrossBleAdapter.Current.Status != AdapterStatus.PoweredOn ) {
                DisplayAlert ("Attenzione", "Bluetooth disabilitato", "ok" );
                return;
            }
            if ( CrossBleAdapter.Current.IsScanning ) return;


            // attiva la scansione dei dispositivi
            CrossBleAdapter.Current.Scan( 
                new ScanConfig() { 
                    ScanType = BleScanType.Balanced,
                    //ServiceUuids = 
                }).Subscribe( scanResult => {

                    

//Debug.WriteLine($"Find new device: {device.Name} id: {device.Uuid}");

//                    if ( device.Name=="GEOX_XLED" && device.Status == ConnectionStatus.Connected) {
//                        //CrossBleAdapter.Current.SetAdapterState(false); // or false to disable
//                        device.WhenAnyCharacteristicDiscovered()
//                            .Subscribe(async characteristic => {
////Debug.WriteLine($"Find characteristic: {characteristic.Properties.} id: {device.Uuid}");
//                                //// read, write, or subscribe to notifications here
//                                //var result = await characteristic.Read(); // use result.Data to see response
//                                //await characteristic.Write(bytes);
//                                //characteristic.EnaableNotifications();
//                                //characteristic.WhenNotificationReceived().Subscribe(result => {
//                                // //result.Data to get at response
//                                //});
//                            });

//                    } else {
//                        device.Connect()
//                        .Subscribe( x => {
//                            device.WhenAnyCharacteristicDiscovered()
//                                .Subscribe(async characteristic => {
//Debug.WriteLine($"Find characteristic: {characteristic.Description} id: {characteristic.Uuid}");
//                                    if ( characteristic.Properties == CharacteristicProperties.WriteNoResponse ) {
//                                        characteristic.Write ( Encoding.ASCII.GetBytes("fe15190100000000000000000000007030000000000000000000000031") );
//                                    }
//                                    //// read, write, or subscribe to notifications here
//                                    //var result = await characteristic.Read(); // use result.Data to see response
//                                    //await characteristic.Write(bytes);
//                                    //characteristic.EnableNotifications();
//                                    //characteristic.WhenNotificationReceived().Subscribe(result => {
//                                    // //result.Data to get at response
//                                    //});
//                                });
//                        });
//                    }

                });


        }



        private async void Server_Clicked(object sender, EventArgs evn) {
            
            if ( CrossBleAdapter.Current.Status != AdapterStatus.PoweredOn ) return;


            var server = CrossBleAdapter.Current.CreateGattServer();

            var service = server.AddService(Guid.NewGuid(), true);

            var characteristic = service.AddCharacteristic(
                Guid.NewGuid(),
                CharacteristicProperties.Read | CharacteristicProperties.Write,
                GattPermissions.Read | GattPermissions.Write
            );

            var notifyCharacteristic = service.AddCharacteristic (
                Guid.NewGuid(),
                CharacteristicProperties.Indicate | CharacteristicProperties.Notify,
                GattPermissions.Read | GattPermissions.Write
            );

            IDisposable notifyBroadcast = null;
            notifyCharacteristic.WhenDeviceSubscriptionChanged()
                .Subscribe( e => {
                    var @event = e.IsSubscribed ? "Subscribed" : "Unsubcribed";

                    if (notifyBroadcast == null) {
                        //this.notifyBroadcast = Observable
                        //    .Interval(TimeSpan.FromSeconds(1))
                        //    .Where(x => notifyCharacteristic.SubscribedDevices.Count > 0)
                        //    .Subscribe ( _ => {
                        //        Debug.WriteLine("Sending Broadcast");
                        //        var dt = DateTime.Now.ToString("g");
                        //        var bytes = Encoding.UTF8.GetBytes(dt);
                        //        notifyCharacteristic.Broadcast(bytes);
                        //    });
                    }
                });

            characteristic.WhenReadReceived()
                .Subscribe(x => {
                    var write = "HELLO";
                    // you must set a reply value
                    x.Value = Encoding.UTF8.GetBytes(write);
                    x.Status = GattStatus.Success; // you can optionally set a status, but it defaults to Success
                });

            characteristic.WhenWriteReceived()
                .Subscribe(x => {
                    var write = Encoding.UTF8.GetString(x.Value, 0, x.Value.Length);
                    // do something value
                });
            
            await server.Start(new Plugin.BluetoothLE.Server.AdvertisementData { LocalName = "TestServer" });
        }


        private void DevicesList_ItemSelected(object sender, SelectedItemChangedEventArgs e) {

        }
    }
}
