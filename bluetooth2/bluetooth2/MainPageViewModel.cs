﻿using Plugin.BluetoothLE;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;



namespace bluetooth2 {
    public class MainPageViewModel : INotifyPropertyChanged {



        #region CONSTRUCTOR

        public MainPageViewModel () {
            Devices = new ObservableCollection<IDevice>();
        }

        #endregion



        #region PROPERTIES

        public ObservableCollection<IDevice> Devices {
            get => _Devices;
            set {
                if ( value == _Devices ) return;
                _Devices = value;
                this.OnPropertyChanged();
            }
        }
        ObservableCollection<IDevice> _Devices;
        

        public Plugin.BluetoothLE.IDevice DeviceSelect {
            get => _DeviceSelect;
            set {
                if ( value == _DeviceSelect ) return;
                _DeviceSelect = value;
                this.OnPropertyChanged();
            }
        }
        IDevice _DeviceSelect;


        public bool InScan {
            get => _InScan;
            set {
                if ( value == _InScan ) return;
                _InScan = value;
                if ( _InScan ) ScanStart(); else ScanStop();
                this.OnPropertyChanged();
            }
        }
        bool _InScan = false;

        #endregion



        #region COMMAND
        
        public ICommand StartScanCommand {
            get => new Command( () => {

                // prerequisiti
                if ( CrossBleAdapter.Current.Status != AdapterStatus.PoweredOn ) {
                    MessagingCenter.Send ( this, "AlertWarningPowerOff" );
                    return;
                }
                if ( CrossBleAdapter.Current.IsScanning ) return;
                
                // attiva la scansione dei dispositivi
                InScan = true;
            });
        }

        public ICommand SelectDeviceCommand {
            get => new Command( async par => {

                // se è il servizio GEOX...
                var vm = par as MainPageViewModel;
                var device = vm.DeviceSelect;
                if (device.Name != "GEOX_XLED" || device.Status != ConnectionStatus.Disconnected) return;

                // connessione con il device
                await DeviceConnetc( device );

                await PairingRequest( device );

                // (opzionale) cerco e imposto delle caratteristiche
                FindAndWriteCharacteristics ( device );

                // prelevo il descrittore
                var descriptor = await GetDescriptor(device);

                // ci scrivo dentro
                descriptor.Write(StringToByteArray("fe180100e7"));

            });
        }

        #endregion
        


        #region SCAN DEVICES

        IDisposable SubScan;

        void ScanStart () {
            ScanStop();
            SubScan = CrossBleAdapter.Current.Scan( new ScanConfig() { ScanType = BleScanType.Balanced,} )
                .Subscribe( scanResult => {
                    var adata = scanResult.AdvertisementData;
                    var device = scanResult.Device;

                    // se il device non è in lista lo inserisce
                    if ( !Devices.Any( d => d.Uuid.ToString().Equals(device.Uuid.ToString(), StringComparison.InvariantCultureIgnoreCase)) ) { 
Debug.WriteLine ( $"Device : find : {device.Uuid}" );
                        Devices.Add ( device );
                    };
                });
        }

        void ScanStop () {
            if ( SubScan == null ) return;
            SubScan.Dispose();
        }

        #endregion



        #region CONNECTION

        IDisposable SubConnection;

        Task<bool> DeviceConnetc ( IDevice device ) {

            var taskSource = new TaskCompletionSource<bool> ();

            SubConnection = device.Connect().Subscribe( x => { 
Debug.WriteLine ( $"Device : GEOX_XLED : Connected!" );
                taskSource.SetResult(true); 
            });

            return taskSource.Task;
        }

        void DeviceDisconnect ( IDevice device ) {
            device.CancelConnection();
        }

        #endregion




        Task<IGattDescriptor> GetDescriptor ( IDevice device ) {

            var taskSource = new TaskCompletionSource<IGattDescriptor> ();
            
            IDisposable observer = null;
            observer = device.WhenAnyDescriptorDiscovered()
                .Subscribe ( descriptor => { 
                    if ( observer != null ) observer.Dispose();
                    taskSource.SetResult(descriptor); 
                });

            return taskSource.Task;
        }


        void FindAndWriteCharacteristics ( IDevice device ) {
            // cerco le caratteristiche di questo device
            IDisposable observer = null;
            observer = device.WhenAnyCharacteristicDiscovered()
                .Subscribe( characteristic => {
Debug.WriteLine ( $"Characteristic : find : {characteristic.Description} id: {characteristic.Uuid} properties: {characteristic.Properties.ToString()}" );

                    switch ( characteristic.Properties ) { 
                        case CharacteristicProperties.WriteNoResponse:
                            var value = "fe15190100000400000100000000020200000000";
Debug.WriteLine ( $"Characteristic : write-no-response: {value}" );
                            characteristic.WriteWithoutResponse(StringToByteArray(value));
                            if ( observer!=null ) observer.Dispose();
                        break;
                        case CharacteristicProperties.Read:
                            characteristic.Read().Subscribe ( res => { 
Debug.WriteLine ( $"Characteristic : read : {res.Data}" );
                            });
                            if ( observer!=null ) observer.Dispose();
                        break;
                    }
                });
        }


        Task<bool> PairingRequest(IDevice device) {
            if ( /*!device.IsPairingRequestSupported ||*/ device.PairingStatus == PairingStatus.Paired) Task.FromResult<bool>(true);

            var taskSource = new TaskCompletionSource<bool>();
            device.PairingRequest("111111")
                .Subscribe(x => {
                    taskSource.SetResult(true);
                });

            return taskSource.Task;
        }






        #region UTIL

        static byte[] StringToByteArray(string hex) {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        #endregion



        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
